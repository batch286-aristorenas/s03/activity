from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse
from .models import GroceryItem
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {'groceryitem_list' : groceryitem_list}
							# app folder name
	return render(request, "django_practice/index.html", context)
	#return HttpResponse("Hello from the views.py file")



def groceryitem(request, groceryitem_id):
		groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
		# response = "You are viewing the details of %s"
		# return HttpResponse(response %groceryitem_id)
		return render(request, "django_practice/groceryitem.html", groceryitem)


def register(request):

	users = User.objects.all()
	is_user_registered = False
	context = {
		"is_user_registered" : is_user_registered
	}

	# for loop
	# check if a username "johndoe" already exist
	# if it exist change the value of variable is_user_registered to True
	for indiv_user in users:
		if indiv_user.username == "gambit":
			is_user_registered = True
			break;

	if is_user_registered == False:
		user = User() # empty object
		user.username = "gambit"
		user.first_name = "Remy"
		user.last_name = "Lebeau"
		user.email = "gambit@mail.com"

		#password hashing
		# The set_password is used to ensure that the pw is hashed using Django's authentication framework
		user.set_password("Gambit1234")
		user.is_staff = False
		user.is_active = True
		user.save()


		context = {
			"first_name" : user.first_name,
			"last_name" : user.last_name
		}


	return render(request, "django_practice/register.html", context)


def change_password(request):

	is_user_authenticated = False

	user = authenticate(username="gambit", password="Gambit1234")
	print(user)
	if user is not None:
	    authenticated_user = User.objects.get(username='gambit') # this is where we get the user with username 'John Doe'
	    authenticated_user.set_password("gambit1234")
	    authenticated_user.save()
	    is_user_authenticated = True
	context = {
	    "is_user_authenticated": is_user_authenticated
	}

	return render(request, "django_practice/change_password.html", context)


def login_view(request):
	username = "gambit"
	password = "gambit1234"
	user = authenticate(username=username, password=password)
	context = {
		"is_user_authenticated" : False
	}
	print(user)
	if user is not None:
		login(request,user)
		return redirect("index")
	else:
								# templates/todolist/login.html
		return render(request, "django_practice/login.html", context)


def logout_view(request):
	logout(request)
	return redirect("index")